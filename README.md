# userbr
userbr（User behavior record）
中文简称用户行为记录的js插件工具。
采用MIT协议，任何个人或企业可以免费商用。

这是一个网页用户行为记录插件

最终的结果是生成一个二维数组

# 如何下载

项目支持GitHub、npm、gitee下载

# 代码类型
使用 JavaScript 语言

# 它有什么作用

它允许开发工程师、数据分析师和各种数据相关人员分析用户访问网页的行为和操作数据

# 案例展示
 ![输入图片说明](http://www.quanzhangongchengshi.com/gitee/demo.gif "在这里输入图片标题")
 
 当用户将鼠标移到元素上时，将记录此信息。例如，当用户多次将鼠标移动到价格区域时，我们应该考虑是否降低销售价格，以提高聊天时的交易成功率！
# 案例代码

```
<!DOCTYPE html>
<html lang="zh">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title></title>
		<style type="text/css">
			.nav{
				width: 100%;
				height: 80px;
				background-color: #eee;
			}
			.pay{
				width: 30%;
				height: 80px;
				background-color: #f00;
			}
			.footer{
				width: 100%;
				height: 80px;
				background-color: #f1f1f1;
			}
		</style>
	</head>
	<body>
		<ul>
			<div class="nav userbr" data-cc = 'nav'>
				this is nav
			</div>
			<div class="pay userbr" data-cc = 'pay'>
				this is pay
			</div>
			<div class="footer userbr" data-cc = 'footer'>
				this is footer
			</div>
			<button type="button" id="result">result</button>
		</ul>
		<script type="text/javascript">
			document.getElementById('result').addEventListener('click', function() {
				console.log(userbr_list)
			})
		</script>
	</body>
		<script src="userbr.js" type="text/javascript" charset="utf-8"></script>
	
</html>

```


# 参数说明

 ![输入图片说明](http://www.quanzhangongchengshi.com/gitee/introduce.png "在这里输入图片标题")
 
class = userbr

说明：要记录的元素

data-cc = 自定义

解释：描述这条记录

# 操作流程

![输入图片说明](http://www.quanzhangongchengshi.com/gitee/Process.jpg "在这里输入图片标题")

 
用户将鼠标移到元素上，前端开发人员将数据发往服务器，后端或数据分析人员对数据进行分析，将结果返回给客服或者前端开发者，返回给客服则可促进成交率，返回给前端开发者或UI设计，可以对网站进行进一步的优化 。根据用户的交互信息作为一定的参考数据，不用摸石头过河。

# 获取结果

![输入图片说明](http://www.quanzhangongchengshi.com/gitee/result.png "在这里输入图片标题")
 
 只需输出变量 userbr_list

# 作者提示

作者QQ：1316590732

如果你通过这个插件获利，希望你能给作者一些奖励支持。谢谢你。

![输入图片说明](http://www.quanzhangongchengshi.com/gitee/wxqrcode.png "在这里输入图片标题")